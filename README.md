# Analyzing the Impact of Cyberattacks on Industrial Control Systems using Timed Automata
This repository contains files used to model a manufacturing cell control system (MCCS) using timed automata and perform experiments with different tampering and spoofing attacks described in an eponymous research paper.

## Table of contents
* [Description](#description)
* [Technologies](#technologies)
* [Files](#files)
* [Setup](#setup)

### Description
This project is an effort towards using fromal methods to analyze the real-time impact of cyberattacks on industrial control systems by modeling attackers and the system as networks of timed automata.
	
### Technologies
Project is created with:
* [UPPAAL 4.1.25-5](https://uppaal.org/)

### Files
* __MCCS.xml__: The model file that contains all the described system agents and attackers.
* __MCCS.q__: Query file containing all the queries for classical and statistical model checking.
	
### Setup
To run this project, [download](https://uppaal.org/downloads/) and install UPPAAL 4.1.24 (stable) or 4.1.25-5 (both available under a free academic licence). If an error is encountered, run the executable Jar file (uppaal.jar) from the command line instead of the normal executable file (e.g., uppaal.exe).
1. Open the model file (MCCS.xml) in UPPAAL
2. Import the query file (MCCS.q) if the verifier is empty

